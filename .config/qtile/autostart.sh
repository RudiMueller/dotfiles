#!/bin/sh

# start polkit agent
lxqt-policykit-agent &
# start all XDG autostart programs
dex -a

# start special services
dunst &
