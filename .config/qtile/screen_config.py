from libqtile import bar, widget, qtile
from libqtile.config import Screen

from Xlib import display as xdisplay

from helpers import status_power, get_datetime, volume_get, exit_script

def get_num_monitors():
    num_monitors = 0
    try:
        display = xdisplay.Display()
        screen = display.screen()
        resources = screen.root.xrandr_get_screen_resources()

        for output in resources.outputs:
            monitor = display.xrandr_get_output_info(output, resources.config_timestamp)
            preferred = False
            if hasattr(monitor, "preferred"):
                preferred = monitor.preferred
            elif hasattr(monitor, "num_preferred"):
                preferred = monitor.num_preferred
            if preferred and monitor.crtc:
                num_monitors += 1
    except Exception:
        # always setup at least one monitor
        return 1
    else:
        return num_monitors


def configure_main_screen():
    return Screen(
        top=bar.Bar(
            [
                widget.CurrentLayoutIcon(),
                widget.GroupBox(),
                # widget.Prompt(),
                widget.TaskList(),
                # widget.Chord(
                #    chords_colors={
                #        "launch": ("#ff0000", "#ffffff"),
                #    },
                #    name_transform=lambda name: name.upper(),
                # ),
                # NB Systray is incompatible with Wayland, consider using StatusNotifier instead
                # widget.StatusNotifier(),
                # widget.TextBox("]", foreground="#d75f5f"),
                widget.Sep(),
                # widget.Notify(default_timeout=10),
                widget.Sep(),
                widget.BatteryIcon(
                    mouse_callbacks={
                        "Button1": lambda: qtile.spawn(status_power)
                    }),
                widget.Backlight(backlight_name='intel_backlight',
                                 change_command='brightnessctl s {0}',
                                 fmt='[ {}]'),
                widget.Sep(),
                widget.Systray(),
                widget.Sep(),
                widget.GenPollText(func=get_datetime, update_interval=1),
                widget.Sep(),
                widget.CheckUpdates(distro='Arch_checkupdates',
                                    fmt=' {}',
                                    no_update_string='✔',
                                    initial_text='',
                                    display_format='{updates}',
                                    colour_have_updates="#ff2222",
                                    colour_no_updates="#22ff22"),
                widget.Sep(),
                widget.GenPollText(func=volume_get, update_interval=1),
                widget.Sep(),
                widget.TextBox("[Exit]",
                               mouse_callbacks={
                                   "Button1":
                                   lambda: qtile.spawn(exit_script)
                               }),
            ],
            24,
            # border_width=[2, 0, 2, 0],  # Draw top and bottom borders
            # border_color=["ff00ff", "000000", "ff00ff", "000000"]  # Borders are magenta
        ),
        wallpaper='/usr/share/backgrounds/archlinux/archwave.png',
        wallpaper_mode='stretch',
    )


def configure_screens(screens):
    screens.clear()
    screens.append(configure_main_screen())

    num_monitors = get_num_monitors()

    if num_monitors > 1:
        for m in range(num_monitors - 1):
            screens.append(
                Screen(
                    top=bar.Bar(
                        [
                            widget.GroupBox(),
                            widget.TaskList(),
                        ],
                        24,
                    ),
                    wallpaper='/usr/share/backgrounds/archlinux/archwave.png',
                    wallpaper_mode='stretch',
                )
            )

