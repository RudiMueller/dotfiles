from libqtile import layout, hook
from libqtile.config import Click, Drag, Group, Key, Match
from libqtile.lazy import lazy
# from libqtile.log_utils import logger

import os
import signal
import subprocess

from helpers import (terminal, file_manager, program_launcher,
                     select_emoji, run_calculator, exit_script, screen_script,
                     volume_up, volume_down, volume_mute)
from screen_config import configure_screens

mod = "mod4"


keys = [
    # A list of available commands that can be bound to keys can be found
    # at https://docs.qtile.org/en/latest/manual/config/lazy.html
    # Switch between windows
    Key([mod], "h", lazy.layout.left(), desc="Move focus to left"),
    Key([mod], "l", lazy.layout.right(), desc="Move focus to right"),
    Key([mod], "j", lazy.layout.down(), desc="Move focus down"),
    Key([mod], "k", lazy.layout.up(), desc="Move focus up"),
    Key([mod],
        "space",
        lazy.layout.next(),
        desc="Move window focus to other window"),
    # Move windows between left/right columns or move up/down in current stack.
    # Moving out of range in Columns layout will create new column.
    Key([mod, "shift"],
        "h",
        lazy.layout.shuffle_left(),
        desc="Move window to the left"),
    Key([mod, "shift"],
        "l",
        lazy.layout.shuffle_right(),
        desc="Move window to the right"),
    Key([mod, "shift"],
        "j",
        lazy.layout.shuffle_down(),
        desc="Move window down"),
    Key([mod, "shift"], "k", lazy.layout.shuffle_up(), desc="Move window up"),
    # Grow windows. If current window is on the edge of screen and direction
    # will be to screen edge - window would shrink.
    Key([mod, "control"],
        "h",
        lazy.layout.grow_left(),
        desc="Grow window to the left"),
    Key([mod, "control"],
        "l",
        lazy.layout.grow_right(),
        desc="Grow window to the right"),
    Key([mod, "control"],
        "j",
        lazy.layout.grow_down(),
        desc="Grow window down"),
    Key([mod, "control"], "k", lazy.layout.grow_up(), desc="Grow window up"),
    Key([mod], "n", lazy.layout.normalize(), desc="Reset all window sizes"),
    # Toggle between split and unsplit sides of stack.
    # Split = all windows displayed
    # Unsplit = 1 window displayed, like Max layout, but still with
    # multiple stack panes
    Key(
        [mod, "shift"],
        "Return",
        lazy.layout.toggle_split(),
        desc="Toggle between split and unsplit sides of stack",
    ),
    Key([mod], "Return", lazy.spawn(terminal), desc="Launch terminal"),
    # Toggle between different layouts as defined below
    Key([mod], "Tab", lazy.next_layout(), desc="Toggle between layouts"),
    Key([mod, "shift"], "q", lazy.window.kill(), desc="Kill focused window"),
    Key([mod, "control"], "r", lazy.reload_config(), desc="Reload the config"),
    Key([mod, "control"], "q", lazy.shutdown(), desc="Shutdown Qtile"),
    Key([mod],
        "r",
        lazy.spawn(program_launcher),
        desc="Spawn a command using rofi"),
    Key([mod], "i", lazy.spawn(select_emoji), desc="Copy an Emoji"),
    Key([mod], "c", lazy.spawn(run_calculator), desc="Open rofi calculator"),
    Key([mod], "e", lazy.spawn(file_manager), desc="Open file manager"),
    Key([mod], "a", lazy.spawn(exit_script), desc="Spawn exit dialog"),
    Key([mod], "s", lazy.spawn(screen_script), desc="Spawn screen configuration dialog"),
    Key([mod],
        "f",
        lazy.window.toggle_fullscreen(),
        desc="Toggle full screen mode"),
    # switch focus to other screen
    Key([mod, "control"], "n", lazy.next_screen()),
    # Key([mod, "control"], "p", lazy.prev_screen()),

    # Volume and Backlight keys
    Key([],
        "XF86AudioLowerVolume",
        lazy.function(volume_down),
        desc="Lautstärke reduzieren"),
    Key([],
        "XF86AudioRaiseVolume",
        lazy.function(volume_up),
        desc="Lautstärke erhöhen"),
    Key([], "XF86AudioMute", lazy.function(volume_mute),
        desc="Stumm schalten"),
    Key([],
        "XF86MonBrightnessDown",
        lazy.spawn("brightnessctl set 5%-"),
        desc="Helligkeit reduzieren"),
    Key([],
        "XF86MonBrightnessUp",
        lazy.spawn("brightnessctl set +5%"),
        desc="Helligkeit erhöhen"),
]

groups = [Group(i) for i in "1234567890"]

for i in groups:
    keys.extend([
        # mod1 + letter of group = switch to group
        Key(
            [mod],
            i.name,
            lazy.group[i.name].toscreen(),
            desc="Switch to group {}".format(i.name),
        ),
        # mod1 + shift + letter of group = switch to & move focused window to group
        Key(
            [mod, "shift"],
            i.name,
            lazy.window.togroup(i.name, switch_group=True),
            desc="Switch to & move focused window to group {}".format(i.name),
        ),
        # Or, use below if you prefer not to switch to that group.
        # # mod1 + shift + letter of group = move focused window to group
        # Key([mod, "shift"], i.name, lazy.window.togroup(i.name),
        #     desc="move focused window to group {}".format(i.name)),
    ])

color_fg = "#999999"
color_bg = "#222222"
color_border = "#0b5394"
color_border_focus = "#2986cc"

layouts = [
    layout.Columns(
        border_focus=color_border_focus,
        border_focus_stack=[color_border_focus, "#2380c6"],
        border_width=0,
        border_normal=color_border,
        margin=2,
    ),
    layout.Max(),
    # Try more layouts by unleashing below layouts.
    # layout.Stack(num_stacks=2),
    # layout.Bsp(),
    # layout.Matrix(),
    # layout.MonadTall(),
    # layout.MonadWide(),
    # layout.RatioTile(),
    # layout.Tile(),
    # layout.TreeTab(),
    # layout.VerticalTile(),
    # layout.Zoomy(),
]

widget_defaults = dict(font="JetBrains Mono Nerd Font",
                       fontsize=12,
                       padding=3,
                       foreground=color_fg,
                       background=color_bg)
extension_defaults = widget_defaults.copy()


screens = []
configure_screens(screens)


# Drag floating layouts.
mouse = [
    Drag([mod],
         "Button1",
         lazy.window.set_position_floating(),
         start=lazy.window.get_position()),
    Drag([mod],
         "Button3",
         lazy.window.set_size_floating(),
         start=lazy.window.get_size()),
    Click([mod], "Button2", lazy.window.bring_to_front()),
]

dgroups_key_binder = None
dgroups_app_rules = []  # type: list
follow_mouse_focus = True
bring_front_click = False
cursor_warp = False
floating_layout = layout.Floating(float_rules=[
    # Run the utility of `xprop` to see the wm class and name of an X client.
    *layout.Floating.default_float_rules,
    Match(wm_class="confirmreset"),  # gitk
    Match(wm_class="makebranch"),  # gitk
    Match(wm_class="maketag"),  # gitk
    Match(wm_class="ssh-askpass"),  # ssh-askpass
    Match(title="branchdialog"),  # gitk
    Match(title="pinentry"),  # GPG key password entry
])
auto_fullscreen = True
focus_on_window_activation = "smart"
reconfigure_screens = True

# If things like steam games want to auto-minimize themselves when losing
# focus, should we respect this or not?
auto_minimize = False

# When using the Wayland backend, this can be used to configure input devices.
wl_input_rules = None

# XXX: Gasp! We're lying here. In fact, nobody really uses or cares about this
# string besides java UI toolkits; you can see several discussions on the
# mailing lists, GitHub issues, and other WM documentation that suggest setting
# this string if your java app doesn't work correctly. We may as well just lie
# and say that we're a working one by default.
#
# We choose LG3D to maximize irony: it is a 3D non-reparenting WM written in
# java that happens to be on java's whitelist.
wmname = "LG3D"


# autostart
@hook.subscribe.startup_once
def autostart():
    autostart_script = os.path.expanduser('~/.config/qtile/autostart.sh')
    subprocess.Popen([autostart_script])


# handle change in active screens
@hook.subscribe.screen_change
def restart_on_randr(event):
    # Qtile reloads the configuration on the USR1 signal. That is the only option to enforce proper reloading
    # without crashes or overlaying groups on single screens.
    os.kill(os.getpid(), signal.SIGUSR1)

