from libqtile.utils import guess_terminal

import os
import subprocess

terminal = guess_terminal()

file_manager = "pcmanfm"
program_launcher = "rofi -show drun -show-icons"
ssh_hosts = "rofi -show ssh"
select_emoji = "rofi -show emoji"
run_calculator = "rofi -show calc -no-show-match"
exit_script = os.path.expanduser('~/.scripts/ui/prompt-shutdown.sh')
status_power = os.path.expanduser('~/.scripts/ui/status.sh power')
screen_script = os.path.expanduser('~/.scripts/ui/prompt-screen.sh')


def get_date():
    return ' ' + subprocess.check_output(['/usr/bin/date', '+%d.%m.%Y %a'
                                           ]).decode('utf-8').strip()


def get_time():
    return ' ' + subprocess.check_output(['/usr/bin/date', '+%H:%M'
                                           ]).decode('utf-8').strip()


def get_datetime():
    return get_date() + ' ' + get_time()


def volume_get():
    return subprocess.check_output(
        ['wpctl', 'get-volume',
         '@DEFAULT_AUDIO_SINK@']).decode('utf-8').strip()


def volume_mute(qt):
    return subprocess.check_output(
        ['wpctl', 'set-volume', '@DEFAULT_AUDIO_SINK@',
         'toggle']).decode('utf-8').strip()


def volume_up(qt):
    return subprocess.check_output(
        ['wpctl', 'set-volume', '@DEFAULT_AUDIO_SINK@',
         '2%+']).decode('utf-8').strip()


def volume_down(qt):
    return subprocess.check_output(
        ['wpctl', 'set-volume', '@DEFAULT_AUDIO_SINK@',
         '2%-']).decode('utf-8').strip()
