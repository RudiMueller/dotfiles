vim.opt.number = true
vim.opt.relativenumber = true

-- Show relative line number when in command mode and absolute line number in edit mode
local _group = vim.api.nvim_create_augroup("numbertoggle", { clear = true })
vim.api.nvim_create_autocmd({"InsertEnter","BufLeave","FocusLost"}, {
	pattern = "*",
	callback = function()
		vim.opt.relativenumber = false
	end,
	group = _group,
})
vim.api.nvim_create_autocmd({"InsertLeave","BufEnter","FocusGained"}, {
	pattern = "*",
	callback = function()
		vim.opt.relativenumber = true
	end,
	group = _group,
})
