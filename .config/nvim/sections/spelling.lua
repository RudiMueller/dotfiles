vim.opt.spelllang = 'en,de,cjk'

vim.keymap.set('n','<leader>o','<cmd>set spell!<cr>')
