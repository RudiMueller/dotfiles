vim.opt.laststatus = 2

vim.opt.statusline = '%f'             -- path to file
vim.opt.statusline:append(' - ')      -- seperator
vim.opt.statusline:append('%y')       -- filetype
vim.opt.statusline:append('%r')       -- readonly
vim.opt.statusline:append('%m')       -- modified

vim.opt.statusline:append('%=')       -- switch to right side
vim.opt.statusline:append('%l')       -- current line
vim.opt.statusline:append('/')        -- seperator
vim.opt.statusline:append('%L')       -- total lines
