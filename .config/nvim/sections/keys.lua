vim.keymap.set('n','<leader>pj','<cmd>:%!python -m json.tool 2>/dev/null<CR>')
vim.keymap.set('n','<leader>px','<cmd>:%!xmllint --encode UTF-8 --format -<CR>')

-- Change text without putting it into the vim register,
-- see https://stackoverflow.com/q/54255/6064933
vim.keymap.set("n", "c", '"_c')
vim.keymap.set("n", "C", '"_C')
vim.keymap.set("n", "cc", '"_cc')
vim.keymap.set("x", "c", '"_c')
