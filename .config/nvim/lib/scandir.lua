function scandir(directory)
    local i, t, popen = 0, {}, io.popen
    local pfile = popen('ls -1 "'..directory..'"/*.lua')
    for filename in pfile:lines() do
        i = i + 1
        t[i] = filename
    end
    pfile:close()
    return t
end
