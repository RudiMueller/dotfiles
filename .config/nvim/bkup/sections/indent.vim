set tabstop=4       " number of visual spaces per TAB
set softtabstop=4   " number of spaces in tab when editing
set shiftwidth=4    " number of spaces to use for autoindent
set expandtab       " tabs are space
set autoindent
set copyindent      " copy indent from the previous line


" special indentation for YAML files
autocmd FileType yaml setlocal ts=2 sts=2 sw=2 expandtab

" configuration of the indentLine plugin
"let g:indentLine_enabled = 0
"let g:indentLine_char_list = ['|', '¦', '┆', '┊']
let g:indentLine_char = '|'
let g:indentLine_leadingSpaceEnabled = 1
let g:indentLine_leadingSpaceChar = '·'
let g:indentLine_concealcursor = ''
"let g:indentLine_setColors = 0
let g:indentLine_bgcolor_term = 237
