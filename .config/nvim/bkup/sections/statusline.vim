" configuration of the status line

set laststatus=2
set statusline=%f          " Path to the file
set statusline+=\ -\      " Separator
set statusline+=%y        " Filetype of the file
set statusline+=%r        " read only flag
set statusline+=%m        " modified flag

set statusline+=%=        " Switch to the right side
set statusline+=%l    " Current line
set statusline+=/    " Separator
set statusline+=%L   " Total lines

