
call plug#begin('~/.local/share/nvim/plugged')

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'tpope/vim-commentary'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'alvan/vim-closetag'
Plug 'AndrewRadev/tagalong.vim'
Plug 'vimwiki/vimwiki'
Plug 'Yggdroot/indentLine'
Plug 'elzr/vim-json'
Plug 'dense-analysis/ale'
Plug 'jamessan/vim-gnupg', {'branch': 'main'}
Plug 'aliou/bats.vim'

call plug#end()

set t_Co=256
colors zenburn

set cursorline
set colorcolumn=80

let g:vimwiki_list = [{'path': '~/vimwiki/', 'syntax': 'markdown', 'ext': '.md'}]

" YouCompleteMe Short-Cuts
let g:ycm_key_list_select_completion=[]
let g:ycm_key_list_previous_completion=[]

syntax on

"remove useless and stupid conceal from json
let g:vim_json_syntax_conceal = 0

" ignore casing in searches but respect uppercase
set ignorecase
set smartcase

" hide modified buffers
set hidden

" ask before closeing modified buffers
set confirm

set autowrite

" Autocompletion
set wildmode=longest,list,full

" split options
set splitbelow splitright

" enable system clipboard
set clipboard+=unnamedplus

" configure linting
let g:ale_echo_msg_format = '[%linter%] %s [%severity%]'
let g:ale_sign_error = '✘'
let g:ale_sign_warning = '⚠'

" load all configurations from sections
runtime! sections/*.vim

set exrc    
set secure
