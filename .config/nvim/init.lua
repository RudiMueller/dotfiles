local config_path = vim.fn.stdpath("config")
dofile(config_path .. '/lib/scandir.lua')

-- basic cursors
vim.opt.colorcolumn = "80"
vim.opt.cursorline = true

-- case options for search
-- ignore casing but respect uppercase
vim.opt.ignorecase = true
vim.opt.smartcase = true

vim.opt.confirm = true
vim.opt.autowrite = true
vim.opt.splitbelow = true
vim.opt.splitright = true

-- source all the section files
local configs = scandir(config_path .. '/sections')
for _, name in ipairs(configs) do
  local source_cmd = "source " .. name
  vim.cmd(source_cmd)
end

vim.opt.exrc = true
vim.opt.secure = true
