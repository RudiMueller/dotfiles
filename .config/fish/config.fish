if status is-interactive
    # Commands to run in interactive sessions can go here

    # source the profile stuff like a good shell
    fenv source /etc/profile
    fenv source ~/.profile

    # get my default prompt
    starship init fish | source
    # initialize the `direnv` magic
    direnv hook fish | source
end
