#!/bin/sh

VALUE="$2"
SIZE="$1"

_FILLED=$(echo "$SIZE""*"$(echo "$VALUE"|sed 's/.$//')"/100"|bc)
_EMPTY=$(echo "$SIZE""-""$_FILLED"|bc)
_FILLED_STR=$(for i in `seq "$_FILLED"`; do printf "="; done)
_EMPTY_STR=$(for i in `seq "$_EMPTY"`; do printf "_"; done)
printf "[%s%s] %s" "$_FILLED_STR" "$_EMPTY_STR" "$VALUE"
