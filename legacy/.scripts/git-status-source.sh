#!/bin/sh

for d in ~/dev/*; do
  if [ "$d" = "*" ]; then continue; fi
  if [ ! -d "$d" ]; then continue; fi
  if [ -L "$d" ]; then continue; fi
  if [ ! -d "$d"/.git ]; then continue; fi
  git --work-tree="$d" --git-dir="$d"/.git fetch
  status=$(git --work-tree="$d" --git-dir="$d"/.git status -bs | grep -E "^#")
  echo "$d" "$status"
done

