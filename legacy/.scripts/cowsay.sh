#!/bin/sh

ls -1 /usr/share/cows/ | sed s/[.]cow// | fzy | xargs -I{} cowsay -f {} "$1"
