#!/bin/sh

case $(echo -e "Sperren\nRuhezustand\nNeu Starten\nHerunterfahren" | rofi -dmenu -i -p ">>") in
    "Sperren") betterlockscreen -l;;
    "Ruhezustand") systemctl suspend;;
    "Neu Starten") sudo shutdown -r now;;
    "Herunterfahren") sudo shutdown -h now;;
esac
