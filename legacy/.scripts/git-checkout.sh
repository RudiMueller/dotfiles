#!/bin/sh

git fetch
{ git tag & git branch -a; } | grep -v "^*" | fzy | sed -E 's|^.*origin/||' | xargs git checkout
