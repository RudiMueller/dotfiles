set t_Co=256
colors zenburn

set cursorline
set colorcolumn=80

call plug#begin('~/.local/share/nvim/plugged')

Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'tpope/vim-commentary'
Plug 'ctrlpvim/ctrlp.vim'
Plug 'alvan/vim-closetag'
Plug 'AndrewRadev/tagalong.vim'

call plug#end()

" YouCompleteMe Short-Cuts
let g:ycm_key_list_select_completion=[]
let g:ycm_key_list_previous_completion=[]

syntax on
" ignore casing in searches but respect uppercase
set ignorecase
set smartcase

" hide modified buffers
set hidden

" ask before closeing modified buffers
set confirm

set autowrite

" Autocompletion
set wildmode=longest,list,full

" split options
set splitbelow splitright

" enable system clipboard
set clipboard+=unnamedplus

" load all configurations from sections
runtime! sections/*.vim

set exrc    
set secure
