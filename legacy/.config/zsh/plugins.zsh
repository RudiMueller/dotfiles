# -*- sh -*-

source /usr/share/zsh/scripts/zplug/init.zsh

# define required plugins

zplug "mafredri/zsh-async", from:"github", use:"async.zsh"

# Install plugins if there are plugins that have not been installed
if ! zplug check --verbose; then
    printf "Install? [y/N]: "
    if read -q; then
        echo; zplug install
    fi
fi

# Then, source plugins and add commands to $PATH
zplug load
