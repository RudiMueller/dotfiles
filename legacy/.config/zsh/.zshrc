
# Add RVM to PATH for scripting. Make sure this is the last PATH variable change.
export PATH="$PATH:$HOME/.rvm/bin"

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

ZSH=${ZSH:-${ZDOTDIR:-$HOME/.config/zsh}}
export ZPLUG_HOME=$ZSH/zplug
source $ZSH/plugins.zsh
source $ZSH/keybinding.zsh
() {
    for config_file ($ZSH/rc/*.zsh) source $config_file
    [ ! -e $ZSH/env ] || . $ZSH/env
}

# VI mode
bindkey -v

# Configure prompt
autoload -Uz promptinit
setopt PROMPT_SUBST
promptinit

_prompt_format_git(){
	if [[ $vcs_info_msg_0_ ]] ; then
		print -n "[$vcs_info_msg_0_] "
	fi
}

PROMPT='%F{green}%n%f@%F{magenta}%m%f %F{blue}%B%~%b%f $(_prompt_format_git)> '
RPROMPT='[%F{yellow}%?%f]'

# Reduce latency when pressing <Esc>
export KEYTIMEOUT=1

#Freeze the terminal
ttyctl -f

source /home/rudolf/.config/broot/launcher/bash/br

# aliases
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'
alias exa='exa --icons -g --group-directories-first --git'
