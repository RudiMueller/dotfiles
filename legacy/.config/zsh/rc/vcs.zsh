# -*- sh -*-

# update vcs_info_msg_0_ properly for other use

[[ $USERNAME != "root" ]] && {
    typeset -gA PRCH
    PRCH=(
        sep $'\uE0B1' end $'\uE0B0'
        retb "" reta $' \u21B5'
        circle $'\u25CF' branch $'\uE0A0'
        ok $'\u2714' ellipsis $'\u2026'
        eol $'\u23CE' running $'\u21BB'
	up $'\u2191' down $'\u2193'
    )
    
    # Async helpers
    _vcs_async_start() {
        async_start_worker vcs_info
        async_register_callback vcs_info _vcs_info_done
    }
    _vcs_info() {
        cd -q $1
        vcs_info
        print ${vcs_info_msg_0_}
    }
    _vcs_info_done() {
        local job=$1
        local return_code=$2
        local stdout=$3
        local more=$6
        if [[ $job == '[async]' ]]; then
            if [[ $return_code -eq 2 ]]; then
                # Need to restart the worker. Stolen from
                # https://github.com/mengelbrecht/slimline/blob/master/lib/async.zsh
                _vcs_async_start
                return
            fi
        fi
        vcs_info_msg_0_=$stdout
        [[ $more == 1 ]] || zle reset-prompt
    }

    autoload -Uz vcs_info

    zstyle ':vcs_info:*' enable git
    zstyle ':vcs_info:*+*:*' debug false
    () {
        local formats="${PRCH[branch]} %b %c%u %m"
	local actionformats="${formats}%{%F{default}%} ${PRCH[sep]} %{%f%}%{%F{green}%}%a%{%f%}"
        zstyle    ':vcs_info:*:*' formats           $formats
        zstyle    ':vcs_info:*:*' actionformats     $actionformats
        zstyle    ':vcs_info:*:*' stagedstr         "%{%F{green}%}${PRCH[circle]}%{%f%}"
        zstyle    ':vcs_info:*:*' unstagedstr       "%{%F{yellow}%}${PRCH[circle]}%{%f%}"
	zstyle -e ':vcs_info:*:*' check-for-changes \
               '[[ $(zstat +blocks $PWD) -ne 0 ]] && reply=( true ) || reply=( false )'

        zstyle ':vcs_info:git*+set-message:*' hooks git-untracked git-st
        function +vi-git-untracked() {
            [[ $(zstat +blocks $PWD) -ne 0 ]] || return
            if [[ $(git rev-parse --is-inside-work-tree 2> /dev/null) == 'true' ]] && \
                git status --porcelain 2> /dev/null | grep -q '??' ; then
                hook_com[staged]+="%{%F{red}%}${PRCH[circle]}%{%f%}"
            fi
        }

        ### git: Show +N/-N when your local branch is ahead-of or behind remote HEAD.
        # Make sure you have added misc to your 'formats':  %m
        function +vi-git-st() {
            local ahead behind
            local -a gitstatus
            [[ $(zstat +blocks $PWD) -ne 0 ]] || return
        
            # for git prior to 1.7
            # ahead=$(git rev-list origin/${hook_com[branch]}..HEAD | wc -l)
            ahead=$(git rev-list ${hook_com[branch]}@{upstream}..HEAD 2>/dev/null | wc -l)
            (( $ahead )) && gitstatus+=( "${PRCH[up]}${ahead}" )
        
            # for git prior to 1.7
            # behind=$(git rev-list HEAD..origin/${hook_com[branch]} | wc -l)
            behind=$(git rev-list HEAD..${hook_com[branch]}@{upstream} 2>/dev/null | wc -l)
            (( $behind )) && gitstatus+=( "${PRCH[down]}${behind}" )
        
            hook_com[misc]+=${(j:/:)gitstatus}
        }
    }

   # Asynchronous VCS status
    async_init
    _vcs_async_start
    add-zsh-hook precmd (){
        async_job vcs_info _vcs_info $PWD
    }
    add-zsh-hook chpwd (){
        vcs_info_msg_0_=
    }
}
