# -*- sh -*-

#Configuration of history
HISTFILE=~/.histfile
HISTSIZE=2048
SAVEHIST=4096
setopt appendhistory autocd extendedglob notify
autoload -Uz up-line-or-beginning-search down-line-or-beginning-search
zle -N up-line-or-beginning-search
zle -N down-line-or-beginning-search

[[ -n "${key[Up]}"   ]] && bindkey -- "${key[Up]}"   up-line-or-beginning-search
[[ -n "${key[Down]}" ]] && bindkey -- "${key[Down]}" down-line-or-beginning-search

# configure autocompletion
zstyle ':completion:*' menu select
setopt COMPLETE_ALIASES
autoload -Uz compinit
compinit
