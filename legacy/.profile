#set XDG dirs
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_CACHE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

export PATH_SCRIPTS="$HOME/.scripts"

#default programs
export EDITOR="nvim"
export TERMINAL="alacritty"
export BROWSER="firefox"
export READER="zathura"
export FILEMANAGER="lf"
export IMAGEVIEWER="sxiv"

#autostart
mpd >/dev/null 2>&1 &

