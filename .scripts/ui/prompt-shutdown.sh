#!/bin/sh

case $(echo "Sperren\nRuhezustand\nNeu Starten\nHerunterfahren" | rofi -dmenu -i -p ">>") in
    "Sperren") light-locker-command -l;;
    "Ruhezustand") systemctl suspend;;
    "Neu Starten") reboot;;
    "Herunterfahren") poweroff;;
esac
