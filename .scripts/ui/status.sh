#!/bin/sh

OPTION="$1"

status_audio() {
    AUDIO_STATUS=""
    AUDIO_LIST=$(amixer -c 0 controls |grep "iface=MIXER" |grep Volume |sed "s/^.*='\(\w\+\).*$/\1/" |grep -v Internal)
    for MIXER in $AUDIO_LIST
    do
        AUDIO_STATUS="$AUDIO_STATUS"$(printf "\n%.23s.%s" "$MIXER..............................." $(amixer -c 0 get "$MIXER"|grep %|sed 's/^.*\(\[[0-9]\+%\]\).*$/\1/'|uniq))
    done
    notify-send "Audio status" "$AUDIO_STATUS"
}

status_network() {
    CON_LIST=$(nmcli device|grep -v "nicht ver"|awk '{print $1}'|grep -v DEVICE)
    for CON in $CON_LIST
    do
        CON_TYPE=$(nmcli device show "$CON" |grep TYPE |awk '{print $2}')
        CON_CONNECTION=$(nmcli device show "$CON" |grep CONNECTION |awk '{print $2}')
        CON_ADDR4=$(nmcli device show "$CON" |grep IP4.ADDRE |awk '{print $2}')
        CON_GW4=$(nmcli device show "$CON" |grep IP4.GATEWAY |awk '{print $2}')
        CON_DNS4=$(nmcli device show "$CON" |grep IP4.DNS |awk '{print $2}')
        notify-send ${CON_TYPE}" verbunden" "Netzwerk: "${CON_CONNECTION}"\nIP:       "${CON_ADDR4}"\nDNS:      "${CON_DNS4}"\nGateway:  "${CON_GW4}
    done
}

case "$OPTION" in
  "power") notify-send "Batteriestatus" "$(acpi -ab)";;
  "date") notify-send "$(date)";;
  "network") status_network;;
  "audio") status_audio;;
  "brightness") $PATH_UI_SCRIPTS/brightness.sh;;
  *)
      echo "usage: "
      echo " status.sh power"
      echo " status.sh date"
      echo " status.sh network"
      echo " status.sh audio"
      echo " status.sh brightness"
      ;;
esac
