#!/bin/sh

ls -1 "${HOME}/.screenlayout/" | rofi -dmenu -only-match -p "Bildschirm-Konfig" | xargs -I{} sh -c "${HOME}/.screenlayout/"{}
