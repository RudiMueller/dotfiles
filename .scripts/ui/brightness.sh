#!/bin/sh

_PROGRESS=$(if [ -z "$1" ]; then brightnessctl; else sudo brightnessctl set "$1"; fi |grep -Eo "[0-9]+%")
notify-send "Bildschirmhelligkeit" "$("$PATH_SCRIPTS/progress.sh" "40" "$_PROGRESS")"
