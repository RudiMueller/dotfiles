#!/bin/bash

DIRS="Bilder Dokumente Downloads Dropbox GPG .jameica .local Musik Nextcloud Öffentlich Projekte .ssh Tools Videos Vorlagen"

for directory in $DIRS;do
	echo "syncing ${directory}..."
	rsync --progress --checksum --no-whole-file --delete --delete-delay -rltgoD "${directory}" rudolf@lager.local.rudis-infrastruktur.de:/mnt/pool/backups/machines/rudolf-johannes_tuxedo
done






