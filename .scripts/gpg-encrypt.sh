#!/bin/sh

MESSAGE_FILE="${1}"
#KEY_SERVER=${2:-pgp.mit.edu}
KEY_SERVER=${2:-keys.openpgp.org}

RECIPIENTS=$(gpg --list-keys --with-colons | grep -E "^uid" | awk -F ":" '{print $10}')
SELECTION=$(echo "${RECIPIENTS}" | fzy -p "Recipient: " )
RECIPIENT=$(echo "${SELECTION}" | sed 's/^\([[:alnum:]]\+\( [[:alnum:]]\+\)\? <\)\?\([^>\n]\+\)>\?$/\3/g')
if [ "$(echo "${RECIPIENTS}" | grep "${SELECTION}")" != "${SELECTION}" ]; then
    gpg --keyserver "${KEY_SERVER}" --search-keys "${RECIPIENT}" || exit 1 
fi

gpg --encrypt --sign --recipient "${RECIPIENT}" "${MESSAGE_FILE}"
