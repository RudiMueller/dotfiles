#!/bin/sh
git fetch
TAGS=$(git tag & git branch -a;)
L=$(echo "$TAGS" | fzy -p "Left:" | sed -E "s|^\*?\s*(remotes/)?||")
R=$(echo "$TAGS" | fzy -p "Right:" | sed -E "s|^\*?\s*(remotes/)?||")
git difftool -t meld --d "$L" "$R"
