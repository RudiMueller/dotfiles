#!/bin/sh
NAME="$1"
openssl ecparam -name secp384r1 -out secp384r1.pem
openssl req -x509 -nodes -days 3650 -newkey ec:secp384r1.pem -keyout "$NAME".key -out "$NAME".crt
openssl pkcs12 -export -in "$NAME".crt -inkey "$NAME".key -out "$NAME".pfx
