#!/bin/sh

git fetch
git branch | grep -v "^*" | fzy | sed -E 's|^\s*||' | xargs git branch -d
