#!/bin/sh
NAME="$1"
openssl req -new -newkey rsa:2048 -days 3650 -nodes -x509 -keyout "$NAME".key -out "$NAME".crt
openssl pkcs12 -export -in "$NAME".crt -inkey "$NAME".key -out "$NAME".pfx
